var stripe = require('stripe')('sk_test_5UtRGE81XUTSO2fuohUg0qWl');
var request = require('request');
var bodyParser = require('body-parser');

module.exports = function (app) {
    var router = app.loopback.Router();
    router.get('/ping', function (req, res) {
        res.send('pongaroo');
    });

    router.get('/getAccessToken', function (req, respond) {

        var code = req.query.code;
        var scope = req.query.scope;

        //if scope is not null or empty then request POST
        if (code) {
            console.log('Code: ' + code);
            console.log('Scope: ' + scope);

            request.post("https://connect.stripe.com/oauth/token", {
                json: {
                    "client_secret": "sk_test_5UtRGE81XUTSO2fuohUg0qWl",
                    "code": code,
                    "grant_type": "authorization_code"
                }
            }, function (error, res, body) {
                if (error) {
                    console.error("Error: " + error)
                    return
                }
                console.log(`statusCode: ${res.statusCode}`)
                console.log(body)

                var access_token = body.access_token;
                var livemode = body.livemode;
                var refresh_token = body.refresh_token;
                var token_type = body.token_type;
                var stripe_publishable_key = body.stripe_publishable_key;
                var stripe_user_id = body.stripe_user_id;
                var scope = body.scope;
                var query = "scope=" + scope + "&stripe_user_id=" + stripe_user_id;

                respond.redirect(`pevail://? + ${query}`);
            });
        } else {
            console.log('Scope is null or empty');
        }//end of else
    });

    router.post('/donate', (req, res) => {

        var amount = req.body.amount;
        var destinationAccount = req.body.destinationAccount;
        console.log("Customer: " + destinationAccount + " Muzammil");

        stripe.charges.create({
            amount: amount,
            currency: "usd",
            source: "tok_visa",
            destination: {
                account: destinationAccount
            }
        }).then(function (charge) {
            res.status(200).send(charge)
        }).catch((err) => {
            console.log(err, req.body)
            res.status(500).end()
        });
    });

    router.post('/create_customer', (req, res) => {

        var description = req.body.name

        stripe.customers.create({
            description: description,
            source: "tok_amex" // obtained with Stripe.js
        }, function (err, customer) {
            // asynchronously called
            if (err) {
                console.log(err, req.body)
                res.status(500).end()
            } else {
                res.status(200).send(customer)
            }
        });

    });


    router.post('/charge', (req, res) => {
        var customer = req.body.customer;
        var amount = req.body.amount;
        var currency = req.body.currency;

        stripe.charges.create({
            customer: customer,
            amount: amount,
            currency: currency
        }, function (err, charge) {
            if (err) {
                console.log(err, req.body)
                res.status(500).end()
            } else {
                res.status(200).send(charge)
            }
        })
    });


    router.post('/ephemeral_keys', (req, res) => {
        var customer_id = req.body.customer_id;
        var api_version = req.body.api_version;

        stripe.ephemeralKeys.create(
            { customer: customer_id },
            { stripe_version: api_version }
        ).then((key) => {
            res.status(200).send(key)
        }).catch((err) => {
            console.log(err, req.body)
            res.status(500).end()
        });
    });

    app.use(router);
}