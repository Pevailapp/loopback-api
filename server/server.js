'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
var LoopBackContext = require('loopback-context');
var path = require('path');

var app = module.exports = loopback();
require('loopback-ds-readonly-mixin')(app);

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use('/express-status', function (req, res, next) {
  res.json({ running: true });
});

app.start = function () {
  // start the web server
  return app.listen(function () {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');

    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;

    }
  });
};





var remotes = app.remotes();
remotes.options.rest = remotes.options.rest || {};
remotes.options.rest.handleErrors = false;
app.middleware('final', FinalErrorHandler);
function FinalErrorHandler(err, req, res, next) {

  if (err) {
    if (err.statusCode != 200) {

      if (err.name == "ValidationError") {


        var allErrorKeys = Object.keys(err.details.codes);



        var presenceError = allErrorKeys.map((key) => {
          try {
            if (err.details.codes[key].includes("presence")) {
              return key;
            }
          } catch (error) {
            return null;
          }
        }).filter((val) => { return val != null });

        var uniquenessError = allErrorKeys.map((key) => {
          try {
            if (err.details.codes[key].includes("uniqueness")) {
              return key;
            }
          } catch (error) {
            return null;
          }
        }).filter((val) => { return val != null });


        var message = presenceError.reduce(
          function (prev, curr, i) {

            return prev + curr + ((i === presenceError.length - 2) ? ' and ' : (i == presenceError.length - 1 ? ' ' : ', '))
          }, '');

        if (presenceError.length != 0) {
          message += (presenceError.length >= 2 ? "are " : "is ") + "Required. ";
        };

        message += uniquenessError.reduce(

          function (prev, curr, i) {
            if (uniquenessError.length == 0) {
              return "";
            }
            return prev + curr + ((i === uniquenessError.length - 2) ? ' and ' : (i == uniquenessError.length - 1 ? ' ' : ', '))
          }, '')
        if (uniquenessError.length != 0) {
          message += (uniquenessError.length > 0 ? "should be Unique" : "");
        };
        // 
        //generating resp

        var error = {
          headers: {
            app_name: "Pevail", version: "1.0", date: new Date().toTimeString()
          },
          status: false,
          message: message

        };
        res.status(err.statusCode).send(error
        ).end();

      } else if (err.name == "Error") {

        var error = {
          headers: {
            app_name: "Pevail", version: "1.0", date: new Date().toTimeString()
          },
          status: false,
          message: err.message

        };


        res.status(err.statusCode).send(error
        ).end();

      }
      else {

        var error = {
          headers: {
            app_name: "Pevail", version: "1.0", date: new Date().toTimeString()
          },
          status: false,
          message: err.message

        };
        res.status(err.statusCode).send(error
        ).end();

      }


    }
  }
  else {


  }
  next();
}

var remotes = app.remotes();
// modify all returned values
remotes.after('**', function (ctx, next) {
  if (ctx) {

    // 
    var data = {
      headers: {
        app_name: "Peval", version: "1.0", date: new Date().toTimeString()
      },
      status: true, data: ctx.result

    };
    ctx.result = data
  }
  next();
});

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function (err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
