var json = {
    "error": {
        "statusCode": 422,
        "name": "ValidationError",
        "message": "The `PevailUser` instance is not valid. Details: `password` can't be blank (value: undefined); `email` can't be blank (value: undefined).",
        "details": {
            "context": "PevailUser",
            "codes": {
                "password": [
                    "presence", "uniqueness"
                ],
                "email": [
                    "presence", "uniqueness"
                ]
            },
            "messages": {
                "password": [
                    "can't be blank"
                ],
                "email": [
                    "can't be blank"
                ]
            }
        }
    }

}




if (json.error.name == "ValidationError") {



    var allErrorKeys = Object.keys(json.error.details.codes);
    //console.log(allErrorKeys);


    var presenceError = allErrorKeys.map((key) => {
        try {
            if (json.error.details.codes[key].includes("presence")) {
                return key;
            }
        } catch (error) {
            return null;
        }
    }).filter((val) => { return val != null });

    var uniquenessError = allErrorKeys.map((key) => {
        try {
            if (json.error.details.codes[key].includes("uniqueness")) {
                return key;
            }
        } catch (error) {
            return null;
        }
    }).filter((val) => { return val != null });


    var message = presenceError.reduce(
        function (prev, curr, i) {
            return prev + curr + ((i === presenceError.length - 2) ? ' and ' : (i == presenceError.length - 1 ? ' ' : ', '))
        }, '') + (presenceError.length > 2 ? "are " : "is ") + "Required. " +

        uniquenessError.reduce(
            function (prev, curr, i) {
                return prev + curr + ((i === uniquenessError.length - 2) ? ' and ' : (i == uniquenessError.length - 1 ? ' ' : ', '))
            }, '') + (uniquenessError.length > 0 ? "should be unique" : "");














}
