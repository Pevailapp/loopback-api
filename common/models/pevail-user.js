'use strict';

var config = require('../../server/config.json');
var path = require('path');
var stripe = require('stripe')('sk_test_5UtRGE81XUTSO2fuohUg0qWl');

var senderAddress = 'newuser@pevail.com';

module.exports = function (Pevailuser) {



    Pevailuser.afterRemote('create', async function (context, user, next) {

        if (user.authUser) {
            next();
        }
        var options = {
            host: 'pevail.us-east-1.elasticbeanstalk.com',
            type: 'email',
            to: user.email,
            from: senderAddress,
            subject: 'Thanks for registering.',
            template: path.resolve(__dirname, '../../server/views/verify.ejs'),
            redirect: '/verified',
            user: user
        };



        try {
            var customer = await stripe.customers.create({
                email: user.email,
                description: 'Customer for jenny.rosen@example.com',
                source: "tok_visa" // obtained with Stripe.js
            });

            user.stripeId = customer.id;
            await user.save();

            const userId = user.id;
            const subModel = Pevailuser.app.models.Subscription;
            const pkgModel = Pevailuser.app.models.Package;
            var pkg = null;
            try {
                pkg = await pkgModel.findOne({
                    where: {
                        title: "FREE"
                    }
                });

                if (pkg == null) throw new Error();


            }
            catch (err) {
                err.message = "before you create aby user , you must have a package titled as FREE";
                err.statusCode = 404;
                throw err;
            }
            try {


                var expiredate = null;
                if (pkg.expireDays != -1) {
                    expiredate = new Date();
                    expiredate.setDate(expiredate.getDate() + pkg.expireDays);

                }
                var sub = await subModel.create({

                    packageId: pkg.title,
                    userId: userId,
                    SubscribeDate: Date.now(),
                    expireDate: expiredate

                });


                var obj = [pkg, sub];

                return obj;
            } catch (error) {


                error.message = "Unknown Error accured";
                error.statusCode = 404;
                throw error;
            }






            await user.verify(options);
        } catch (error) {
            Pevailuser.deleteById(user.id);
            throw next(error);
        }


    });


    Pevailuser.afterRemote('login', async function (context, user, next) {

        await setLoggedIn(user, Pevailuser);



    });


    Pevailuser.beforeRemote('logout', async function (context, user, next) {



        user.loggedIn = true;
        var data = await Pevailuser.find({ where: { _id: user.userId } });


        var data = await Pevailuser.findOne({ where: { _id: user.userId } });

        data.loggedIn = false;
        await data.save();
        next();


    });

    Pevailuser.on('resetPasswordRequest', function (info) {
        var url = 'http://' + config.host + ':' + config.port + '/reset-password';
        var html = 'Click <a href="' + url + '?access_token=' +
            info.accessToken.id + '">here</a> to reset your password';

        Pevailuser.app.models.Email.send({
            to: info.email,
            from: senderAddress,
            subject: 'Password reset',
            html: html
        }, function (err) {
            if (err) return err;// 

        });
    });

    Pevailuser.loginByFacebook = async function (token, user) {


        try {

            var resp = await Pevailuser.app.dataSources.facebook.verifyfb(token, "291241258432163|UeQldy-AgGT_f6imBkNDv1RN-u8");



            if (resp.is_valid == true) {

                var data = await Pevailuser.find({ where: { fbUserId: resp.user_id } });


                if (data.length == 0) {



                    if (user.email == null || user.email == "") {

                        var err = new Error("must include email address");
                        err.statusCode = 400;

                        throw err;
                    }
                    user.password = token;
                    user.authUser = true;
                    user.fbToken = resp.user_id;
                    try {

                        data = await Pevailuser.create(user);

                    }
                    catch (err) {

                        throw err.message;
                    }

                }
                await setLoggedIn(data, Pevailuser);

                var accessToken = await data.createAccessToken(50000);
                return accessToken;

            } else {
                var err = new Error("Cannot verify facebook auth token");
                err.statusCode = 401;
                err.name = "Unauthorized";
                delete err.stack;
                throw err;
            }
        } catch (error) {

            return error;
        }

    }

    Pevailuser.loginByGoogle = async function (GoogleId, user) {




        try {



            var resp = await Pevailuser.app.dataSources.google.verify(GoogleId);



            if (resp.email_verified == true && resp.email == user.email) {

                var data = await Pevailuser.find({ where: { email: resp.email } });


                if (data.length == 0) {



                    if (user.email == null || user.email == "" || user.email != resp.email) {

                        var err = new Error("must include valid email address");
                        err.statusCode = 400;

                        return err;
                    }
                    user.password = resp.sub;
                    user.authUser = true;
                    user.googleToken = resp.sub;
                    try {

                        data = await Pevailuser.create(user);

                    }
                    catch (err) {

                        return err.message;
                    }

                }
                await setLoggedIn(data, Pevailuser);




                var accessToken = await data.createAccessToken(50000);
                return accessToken;

            } else {
                var err = new Error("Cannot verify google auth token");
                err.statusCode = 401;
                err.name = "Unauthorized";
                delete err.stack;
                return err;
            }
        } catch (error) {

            return error;
        }

    }


    Pevailuser.UpdateOneSignalToken = async function (OStoken, options) {


        try {


            const userId = options.accessToken.__data.userId.toString();

            var data = await Pevailuser.findOne({ where: { _id: userId } });

            data.oneSignalToken = OStoken;
            await data.save();

            return true;

        } catch (error) {

            return false;
        }

    }
    Pevailuser.subscribe = async function (packageId, options) {
        const userId = options.accessToken.__data.userId.toString();
        const subModel = Pevailuser.app.models.Subscription;
        const pkgModel = Pevailuser.app.models.Package;
        try {

            await Pevailuser.unsubscribe(options);
        } catch (error) {

        }
        try {
            var pkg = await pkgModel.findOne({
                where: {
                    title: packageId
                }
            });

            if (pkg == null) throw new Error();

            try {


                var expiredate = null;
                if (pkg.expireDays != -1) {
                    expiredate = new Date();
                    expiredate.setDate(expiredate.getDate() + pkg.expireDays);

                }
                var sub = await subModel.create({

                    packageId: pkg.title,
                    userId: userId,
                    SubscribeDate: Date.now(),
                    expireDate: expiredate

                });


                var obj = [pkg, sub];

                return obj;
            } catch (error) {


                error.message = "Unknown Error accured";
                error.statusCode = 404;
                return error;
            }



        }
        catch (err) {

            err.statusCode = 404;
            err.message = "Package Id Not found";
            throw err;
        }




    }

    Pevailuser.getOneSignalToken = async function (options) {

        try {

            const userId = options.accessToken.__data.userId.toString();
            var data = await Pevailuser.findOne({ where: { _id: userId } });


            return data.oneSignalToken;

        } catch (error) {

            return null;
        }

    }


    Pevailuser.unsubscribe = async function (options) {

        try {


            const userId = options.accessToken.__data.userId.toString();

            var sub = await Pevailuser.CurrentSubscription(options);

            if (sub == null) throw new Error();


            sub["expireDate"] = Date.now();
            await sub.save();

            return sub;


        } catch (error) {


            error.message = "You have not subscribed to any package"
            error.statusCode = 404;
            throw error;
        }

    }

    Pevailuser.CurrentSubscription = async function (options) {
        const userId = options.accessToken.__data.userId.toString();
        var subscribeModel = Pevailuser.app.models.subscription;
        try {


            var sub = await subscribeModel.findOne({


                where: {

                    or: [{ expireDate: { gt: new Date(Date.now()).toJSON() } }, { expireDate: null }],

                    userId: { like: userId }

                }
            }

            );


            return sub;
        } catch (error) {
            return null;

        }
    }
    Pevailuser.SubscribedPackage = async function (options) {
        const userId = options.accessToken.__data.userId.toString();
        var sub = await Pevailuser.CurrentSubscription(options);
        var pkg = await Pevailuser.CurrentPackage(userId);
        var resp = [
            pkg, sub

        ];

        return resp;
    }


    Pevailuser.CurrentPackage = async function (userId) {
        var subscribeModel = Pevailuser.app.models.subscription;
        var packageModel = Pevailuser.app.models.Package;
        try {


            var sub = await subscribeModel.findOne({


                where: {

                    or: [{ expireDate: { gt: new Date(Date.now()).toJSON() } }, { expireDate: null }],

                    userId: { like: userId }

                }
            }

            );



            return await packageModel.findOne({ where: { title: sub.packageId } });
        } catch (error) {
            return null;
        }
    }
    Pevailuser.getStripeId = async function (options) {

        try {

            const userId = options.accessToken.__data.userId.toString();
            var data = await Pevailuser.findOne({ where: { _id: userId } });


            return data.stripeId;

        } catch (error) {

            return null;
        }

    }
    Pevailuser.remoteMethod('loginByFacebook', {

        accepts: [
            {
                arg: "token",
                type: "string",
                required: true,
                description: "access token of facebook auth that is provided on login ",
                http: {
                    "source": "query"
                }
            },
            {
                arg: "user",
                type: "PevailUser",
                required: false,
                description: "other user data",
                http: {
                    source: "body"
                }
            }
        ],
        returns: [
            {
                arg: 'body',
                type: 'AccessToken',
                root: true, status: 200
            }
        ],
        description: "Use to login by fb , kindly specify Access token , and other user properties (optional). do not specify password .specify email  provided by facebook  .",
        http: {
            path: "/loginByFacebook",
            verb: "post", status: 200
        }



    });
    Pevailuser.remoteMethod('UpdateOneSignalToken', {

        accepts: [
            {
                arg: "OStoken",
                type: "string",
                required: true,
                description: "onesignal token ",
                http: {
                    "source": "query"
                }
            },
            {
                arg: 'options',
                type: 'object',
                http: 'optionsFromRequest'
            }
        ],
        returns: [
            {
                arg: 'Updated',
                type: 'boolean',
                status: 200
            }
        ],
        description: "Use to uupdate One Signal Token, kindly specify token  .",
        http: {
            path: "/UpdateOneSignalToken",
            verb: "get", status: 200
        }



    });
    Pevailuser.remoteMethod('getOneSignalToken', {

        accepts: [{
            arg: 'options',
            type: 'object',
            http: 'optionsFromRequest'
        }],
        returns: [
            {
                arg: 'token',
                type: "string",
                status: 200
            }
        ],
        description: "Use to retrive  One Signal Token",
        http: {
            path: "/getOneSignalToken",
            verb: "post", status: 200
        }



    });
    Pevailuser.remoteMethod('getStripeId', {

        accepts: [{
            arg: 'options',
            type: 'object',
            http: 'optionsFromRequest'
        }],
        returns: [
            {
                arg: 'stripeId',
                type: "string",
                status: 200
            }
        ],
        description: "Use to retrive stripe id.",
        http: {
            path: "/getStripeId",
            verb: "post", status: 200
        }



    });

    Pevailuser.remoteMethod('subscribe', {

        accepts: [{
            arg: "packageId",
            type: "string",
            required: true,
            description: "Package title or Id ",
            http: {
                "source": "query"
            }
        }, {
            arg: 'options',
            type: 'object',
            http: 'optionsFromRequest'
        }],
        returns: [
            {
                arg: 'package',
                type: "Package",

            },
            {
                arg: 'subscription',
                type: "subscription",

            }
        ],
        description: "Subscribe the user to any package",
        http: {
            path: "/subscribe",
            verb: "post", status: 200
        }



    });

    Pevailuser.remoteMethod('SubscribedPackage', {

        accepts: [{
            arg: 'options',
            type: 'object',
            http: 'optionsFromRequest'
        }],
        returns: [
            {
                arg: 'package',
                type: "Package",

            },
            {
                arg: 'subscription',
                type: "subscription",

            }
        ],
        description: "Subscribe the user to any package",
        http: {
            path: "/SubscribedPackage",
            verb: "post", status: 200
        }



    });
    Pevailuser.remoteMethod('unsubscribe', {

        accepts: [{
            arg: 'options',
            type: 'object',
            http: 'optionsFromRequest'
        }],
        returns: [
            {
                arg: 'subscription',
                type: "subscription",
                status: 200
            }
        ],
        description: "UnSubscribe the user from current package",
        http: {
            path: "/unsubscribe",
            verb: "post", status: 200
        }



    });


    Pevailuser.remoteMethod('loginBygoogle', {

        accepts: [
            {
                arg: "GoogleId",
                type: "string",
                required: true,
                description: "id token of google auth that is provided on login ",
                http: {
                    "source": "query"
                }
            },
            {
                arg: "user",
                type: "PevailUser",
                required: false,
                description: "other user data",
                http: {
                    source: "body"
                }
            }
        ],
        returns: [
            {
                arg: 'body',
                type: 'AccessToken',
                root: true, status: 200
            }
        ],
        description: "Use to login by google , kindly specify Access token , and other user properties (optional). do not specify password .specify email  provided by google .",
        http: {
            path: "/loginBygoogle",
            verb: "post"
        }



    });




};
async function setLoggedIn(user, Pevailuser) {


    user.loggedIn = true;
    var data = await Pevailuser.find({ where: { _id: user.userId } });
    var data = await Pevailuser.findOne({ where: { _id: user.userId } });

    data.loggedIn = true;
    await data.save();
}

