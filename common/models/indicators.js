'use strict';

module.exports = function (Indicators) {


  Indicators.userDetail = async function (userId, options) {
    var CurrentUser = userId + "";
    if (userId == null) {

      CurrentUser = options.accessToken.__data.userId.toString() + "";
    }
    var videos, fans, following;
    var Video = Indicators.app.models.Video;
    try {
      fans = await Indicators.count({
        userId: CurrentUser,
        type: 'follow'
      });

    } catch (error) {
      fans = 0;
    }
    try {
      following = await Indicators.count({
        actor: CurrentUser,
        type: 'follow'
      });

    } catch (error) {
      following = 0;
    }
    try {
      console.log(CurrentUser);
      videos = await Video.count({
        userId: {
          like: CurrentUser
        }

      });
      console.log(videos);
    } catch (error) {
      console.log(error);

      videos = 0;
    }


    return {
      uploads: videos,
      fans: fans,
      following: following
    }
  }
  Indicators.follow = async function (userId, options) {
    var CurrentUser = options.accessToken.__data.userId.toString();





    var filter = {
      and: [

        {
          actor: {
            like: CurrentUser + ""
          }
        },

        {
          userId: {
            like: userId
          }

        },
        {
          type: 'follow'
        }

      ]
    };

    console.log(filter);
    return Indicators.destroyAll(filter).then(r => {
      console.log(r);
      return Indicators.create({
        actor: CurrentUser + "",
        userId: userId,
        type: 'follow'
      }).then(() => {
        return true;
      }).catch(() => {
        return false
      });

    }).catch(c => {
      console.log(c);
      return false
    });


  }
  Indicators.unfollow = async function (userId, options) {
    var CurrentUser = options.accessToken.__data.userId.toString();



    var filter = {
      and: [

        {
          actor: {
            like: CurrentUser + ""
          }
        },

        {
          userId: {
            like: userId
          }

        },
        {
          type: 'follow'
        }

      ]
    };

    console.log(filter);
    return Indicators.destroyAll(filter).then(r => {
      console.log(r);
      return true
    }).catch(c => {
      console.log(c);
      return false
    });

  }


  Indicators.remoteMethod('userDetail', {
    accepts: [{
      arg: 'userId',
      type: 'string',
      http: {
        'source': 'body'
      }
    }, {
      arg: 'options',
      type: 'object',
      http: 'optionsFromRequest'
    }],
    returns: {
      arg: 'Reported',
      type: 'boolean',
      root: 'true'
    },
    http: {
      path: '/userDetail',
      verb: 'post'
    }
  });

  Indicators.updateAboutMe = async function (aboutme, options) {

    var PevailUser = Indicators.app.models.PevailUser;
    var CurrentUser = options.accessToken.__data.userId.toString();
    return PevailUser.update({


      id: CurrentUser


    }, {
        aboutMe: aboutme
      }).then((r) => {
        console.log(r);
        return true;
      }).catch((c) => {
        console.log(c);
        return false;
      })
  }

  Indicators.subscribe = async function (packageName, options) {

    var PevailUser = Indicators.app.models.PevailUser;
    var CurrentUser = options.accessToken.__data.userId.toString();
    return PevailUser.update({


      id: CurrentUser


    }, {
        package: packageName
      }).then((r) => {
        console.log(r);
        return true;
      }).catch((c) => {
        console.log(c);
        return false;
      })
  }
  Indicators.updateDP = async function (imageData, options) {

    var PevailUser = Indicators.app.models.PevailUser;
    var CurrentUser = options.accessToken.__data.userId.toString();
    return PevailUser.update({
      id: CurrentUser
    }, {
        imageUrl: imageData.imageURL
      }).then((r) => {
        console.log(r);
        return true;
      }).catch((c) => {
        console.log(c);
        return false;
      })
  }
  Indicators.getSubscription = async function (options) {

    var PevailUser = Indicators.app.models.PevailUser;
    var CurrentUser = options.accessToken.__data.userId.toString();
    return PevailUser.find({
      where: {


        id: CurrentUser


      }
    }).then((r) => {
      console.log(r);
      return r.map(a => { return a.package }).join(' ');
    }).catch((c) => {
      console.log(c);
      return "";
    })
  }

  Indicators.remoteMethod('follow', {
    accepts: [{
      arg: 'userId',
      type: 'string',
      http: {
        'source': 'body'
      }
    }, {
      arg: 'options',
      type: 'object',
      http: 'optionsFromRequest'
    }],
    returns: {
      arg: 'followed',
      type: 'boolean',
      root: 'true'
    },
    http: {
      path: '/follow',
      verb: 'post'
    }
  });
  Indicators.remoteMethod('unfollow', {
    accepts: [{
      arg: 'userId',
      type: 'string',
      http: {
        'source': 'body'
      }
    }, {
      arg: 'options',
      type: 'object',
      http: 'optionsFromRequest'
    }],
    returns: {
      arg: 'unfollowed',
      type: 'boolean',
      root: 'true'
    },
    http: {
      path: '/unfollow',
      verb: 'post'
    }
  });
  Indicators.remoteMethod('updateAboutMe', {
    accepts: [{
      arg: 'aboutme',
      type: 'string',
      http: {
        'source': 'body'
      }
    }, {
      arg: 'options',
      type: 'object',
      http: 'optionsFromRequest'
    }],
    returns: {
      arg: 'aboutme',
      type: 'boolean',
      root: 'true'
    },
    http: {
      path: '/updateAboutMe',
      verb: 'post'
    }
  });
  Indicators.remoteMethod('updateDP', {
    accepts: [{
      arg: 'imageURL',
      type: 'any',
      http: {
        'source': 'body'
      }
    }, {
      arg: 'options',
      type: 'object',
      http: 'optionsFromRequest'
    }],
    returns: {
      arg: 'unfollowed',
      type: 'boolean',
      root: 'true'
    },
    http: {
      path: '/updateDP',
      verb: 'post'
    }
  });
  Indicators.remoteMethod('subscribe', {
    accepts: [{
      arg: 'packageName',
      type: 'string',
      http: {
        'source': 'body'
      }
    }, {
      arg: 'options',
      type: 'object',
      http: 'optionsFromRequest'
    }],
    returns: {
      arg: 'unfollowed',
      type: 'boolean',
      root: 'true'
    },
    http: {
      path: '/subscribe',
      verb: 'post'
    }
  });


  Indicators.remoteMethod('getSubscription', {
    accepts: {
      arg: 'options',
      type: 'object',
      http: 'optionsFromRequest'
    },
    returns: {
      arg: 'package',
      type: 'string',
      root: 'true'
    },
    http: {
      path: '/getSubscription',
      verb: 'get'
    }
  });
};
