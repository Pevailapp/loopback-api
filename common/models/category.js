'use strict';

module.exports = function (Category) {

    Category.getCategories = async function () {
      try {
        return await Category.find();

      } catch (error) {
        return error;

      }
    }

    Category.remoteMethod('getCategories', {

          returns: {
            arg: 'Categories',
            type: 'any',
            root: "true"
          },
          http: {
            path: '/getCategories',
            verb: 'get'
          }
        });

    };
