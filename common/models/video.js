'use strict';
const OneSignal = require("onesignal-node");

module.exports = async function (Video) {



  Video.report = function (videoId, reason, time_stamp, options) {

    const userId = options.accessToken.__data.userId.toString();

    var indicators = Video.app.models.indicators;

    return indicators.create({
      videoId: videoId,
      actor: userId,
      type: "report",
      reason: reason,
      time_stamp: time_stamp
    }).then(function (params) {
      // 
      console.log("params", params);
      return true;
    }).catch(function (params) {
      return false;
    });
  }



  Video.Fav = async function (videoId, options) {
    var CurrentUser = options.accessToken.__data.userId.toString();
    try {
      var filter = {
        and: [

          {
            videoId: {
              like: videoId
            }
          },

          {
            userId: {
              like: CurrentUser
            }

          },
          {
            type: 'fav'
          }

        ]
      };

      var indicators = Video.app.models.indicators;
      var votes = await indicators.findOne({ where: filter });
      if (votes != null) {

        await indicators.destroyAll(filter)



      }
      else {

        await indicators.create({
          videoId: videoId,
          userId: CurrentUser,
          type: "fav"
        });


      }
      console.log(votes == null);
      var bool = (votes == null);
      return [bool, videoId]


    } catch (error) {
      console.log(error);
      var bool = false;
      return [bool, videoId]
    }


  }
  Video.vote = async function (videoId, options) {
    var CurrentUser = options.accessToken.__data.userId.toString();
    try {
      var filter = {
        and: [

          {
            videoId: {
              like: videoId
            }
          },

          {
            userId: {
              like: CurrentUser
            }

          },
          {
            type: 'vote'
          }

        ]
      };

      var indicators = Video.app.models.indicators;
      var votes = await indicators.findOne({ where: filter });
      if (votes != null) {

        await indicators.destroyAll(filter)



      }
      else {

        await indicators.create({
          videoId: videoId,
          userId: CurrentUser,
          type: "vote"
        });


      }
      console.log(votes == null);
      var bool = (votes == null);
      return [bool, videoId]


    } catch (error) {
      console.log(error);
      var bool = false;
      return [bool, videoId]
    }


  }

  Video.view = function (videoData, options) {


    Video.findOne({
      where: {
        id: videoData.videoId
      }
    }).then((res) => {
      // 
      res.views = res.views + 1;
      res.save();
      return res;
    }).catch((err) => {
      //
      console.log("163 , err", err);
      return err;
    });

  }


  //#region byCategory
  Video.getVideos = async function (req, options) {

    try {
      const CurrentUser = options.accessToken.__data.userId.toString();
      // voila!
      var { videos, filter } = await FilterVideos(req, Video, CurrentUser);

      if (req.search != null) {
        videos = videos.filter((item) => {

          return item.title.toLowerCase().includes(req.search.toLowerCase()) == true;
        })
      }
      var userIds = videos.map((item) => ({
        id: {
          like: item.userId
        }
      }));

      var user = Video.app.models.PevailUser;
      var votedVideos = await GetVotedVideos(videos, filter, CurrentUser);

      var users = await user.find({
        or: userIds
      })

      const result = MapResult(videos, users, votedVideos);
      //composing the result to response of request
      return result;
    } catch (error) {
      error.message = "Error";
      error.statusCode = 404;
      console.log(error);
      throw error;
    }


  };

  async function ValidateIfAllowedToUpload(options) {

    var res = await Video.GetLimit(options);
    if (res.uploaded < res.allowed)
      return true;
    else if (res.allowed == -1)
      return true;
    else return false;

  }

  Video.GetLimit = async function GetLimit(options) {

    const CurrentUser = options.accessToken.__data.userId.toString();

    var user = Video.app.models.PevailUser;
    var cpkg = await user.SubscribedPackage(options)
    var pkg = cpkg[0];
    var sub = cpkg[1];


    try {
      var count = await Video.count({

        and: [
          {
            userId: { like: CurrentUser }
          }, {
            uploadDate: { gte: sub.SubscribeDate }
          }]

      }
      );
    } catch (error) {

    }

    return { uploaded: count, allowed: pkg.MaxUploads };
  }

  Video.upload = async function (video, options) {
    var CurrentUser = options.accessToken.__data.userId.toString();


    if (! await ValidateIfAllowedToUpload(options)) {

      var error = new Error("Max Limit Reached . You cannot upload new videos !");
      error.statusCode = 401;
      throw error;
    }

    try {

      video.userId = CurrentUser;
      video.uploadDate = Date.now();
      let endDate = new Date();
      video.endDate = endDate.setDate(endDate.getDate() + 30);
      await video.save();

      try {

        var indicators = Video.app.models.indicators;

        var notifiable = await indicators.find({
          and: [
            { userId: CurrentUser },
            { type: "follow" }
          ]
          , fields: { actor: true }
        });
        var notifiable = notifiable.map(val => { return val.actor });


        var PevailUser = Video.app.models.PevailUser;
        var PlayerIds = PevailUser.find({ where: { id: { nin: notifiable } } });


        //sending notifications
        var myClient = new OneSignal.Client({
          userAuthKey: 'MWI1NzcwMTktNjhjZC00NTJiLThiYTMtOWZmODZjODBhZDEy',
          app: { appAuthKey: 'MjdiZGNkMWEtN2I1Yi00OTM0LTgzY2UtOGQxM2ExN2YzODUz', appId: '622ffa65-beee-4d33-b300-d58b460e73c3' }
        });

        var firstNotification = new OneSignal.Notification({
          contents: {
            en: "Video Uploaded",
            tr: "Uploaded"
          }, tags: { VideoId: video.id },
          include_player_ids: PlayerIds
        });

        await myClient.sendNotification(firstNotification);
      } catch (error) {
        console.log(error);
      }

      return { video: video };

    } catch (error) {
      error.statusCode = 404;
      console.log(error);
      throw error;
    }

  }

  Video.remoteMethod('report', {
    accepts: [
      {
        arg: 'videoId',
        type: 'any'
      },
      {
        arg: 'reason',
        type: 'any'
      },
      {
        arg: 'time_stamp',
        type: 'any'

      },
      {
        arg: 'options',
        type: 'object',
        http: 'optionsFromRequest'
      }],
    returns: {
      arg: 'Reported',
      type: 'boolean',
      root: "true"
    },
    http: {
      path: '/report',
      verb: 'post'
    }
  });
  Video.remoteMethod('upload', {
    accepts: [{
      arg: 'video',
      type: 'video',
      http: {
        "source": "body"
      }
    }, {
      arg: 'options',
      type: 'object',
      http: "optionsFromRequest"
    }],
    returns: {
      arg: 'video',
      type: 'video',
      root: "true"
    },
    http: {
      path: '/upload',
      verb: 'post'
    }
  });

  Video.remoteMethod('vote', {
    accepts: [{
      arg: 'videoId',
      type: 'string',
      http: {
        "source": "body"
      }
    },
    {
      arg: 'options',
      type: 'object',
      http: "optionsFromRequest"
    }],
    returns: [
      {
        arg: 'isVoted',
        type: 'boolean'
      }, {
        arg: 'videoId',
        type: 'string'
      }
    ],
    http: {
      path: '/vote',
      verb: 'post'
    }
  });
  Video.remoteMethod('GetLimit', {
    accepts: [
      {
        arg: 'options',
        type: 'object',
        http: "optionsFromRequest"
      }],
    returns: [
      {
        arg: 'uploaded',
        type: 'number'
      }
    ],
    http: {
      path: '/GetLimit',
      verb: 'post'
    }
  });


  Video.remoteMethod('Fav', {
    accepts: [{
      arg: 'videoId',
      type: 'string',
      http: {
        "source": "body"
      }
    },
    {
      arg: 'options',
      type: 'object',
      http: "optionsFromRequest"
    }],
    returns: [
      {
        arg: 'isFav',
        type: 'boolean'
      }, {
        arg: 'videoId',
        type: 'string'
      }
    ],
    http: {
      path: '/fav',
      verb: 'post'
    }
  });




  Video.remoteMethod('getVideos', {
    accepts: [{
      arg: 'Filter',
      type: 'VideoFilter',
      http: {
        "source": "body"
      }
    }, {
      arg: 'options',
      type: 'object',
      http: "optionsFromRequest"

    }],
    returns: {
      arg: 'videos',
      type: 'VideoResp',
      root: true
    },
    http: {
      path: '/getVideos',
      verb: 'post'
    }
  });

  Video.remoteMethod('view', {
    accepts: [{
      arg: 'videoId',
      type: 'any',
      http: {
        "source": "body"
      }
    }, {
      arg: 'options',
      type: 'object',
      http: "optionsFromRequest"
    }],
    returns: {
      arg: 'Reported',
      type: 'boolean',
      root: "true"
    },
    http: {
      path: '/view',
      verb: 'post'
    }
  });


};
async function FilterVideos(req, Video, CurrentUser) {


  var filter = {
    where: {}




  };
  if (req.pick) {
    filter["limit"] = req.pick;
  }
  if (req.skip) {

    filter["skip"] = req.skip;
  }
  if (req.category) {
    category: req.category;
    filter.where['category'] = req.category;
  }
  if (req.days != null) {
    var date = new Date();
    date.setDate(date.getDate() - req.days)
    filter.where['uploadDate'] = {


      'gt': date
    }
    console.log(date.toJSON());
    console.log(new Date());
  }


  if (req.starWall) {
    filter.order = ["votes DESC", "views DESC"];
    var now = new Date(Date.now());
    now.setDate(1);
    var start = new Date(now.getFullYear(), now.getMonth() - 1, 1);
    var end = new Date(now.getFullYear(), now.getMonth(), 1);


    console.log(start);
    console.log(end);
    filter.where.and = [];
    filter.where.and.push({
      uploadDate: {


        gte: start
      }
    });

    filter.where.and.push({
      uploadDate: {


        lt: end
      }
    });

  }

  var videos = [];

  videos = await GetReportedVideos(req, Video, CurrentUser, filter);

  console.log(filter);
  // console.log(videos);
  return { videos, filter };
}

async function GetReportedVideos(req, Video, CurrentUser, filter) {
  var videos = {};
  if (!req.includeReported) {
    var indicators = Video.app.models.indicators;
    if (req.includeReported) {
      var reported = await indicators.find({
        where: {
          actor: { like: CurrentUser },
          type: "report"
        }
      });
      filter.where["id"] = {
        nin: reported.map(a => {
          return a.videoId;
        })
      };
    }
    if (req.onlyFav) {
      var fav = await indicators.find({
        where: {
          actor: { like: CurrentUser },
          type: "fav"
        }
      });
      filter.where["id"] = {
        in: fav.map(a => {
          return a.videoId;
        })
      };

    }
    if (req.userId != null) {
      filter.where["userId"] = { like: req.userId };
    }
    else if (req.currentUser) {
      filter.where["userId"] = { like: CurrentUser };
    }
  }
  videos = await Video.find(filter);
  var videosupdated = videos.map(function (video) {
    if (video['endDate'] > new Date()) {
      var remaining = (video['endDate'] - new Date()) / (1000 * 60 * 60 * 24);
      video['remainingDays'] = Math.round(remaining);
    } else {
      video['remainingDays'] = 0;
    }
    return video;
  });
  return videosupdated;
}
async function GetFavVideos(req, Video, CurrentUser, filter) {
  var videos = [];

  var indicators = Video.app.models.indicators;
  var fav = await indicators.find({
    where: {
      actor: { like: CurrentUser },
      type: "fav"
    }
  });
  filter.where["id"] = {
    in: fav.map(a => {
      return a.videoId;
    })
  };

  if (req.userId != null) {
    filter.where["userId"] = { like: req.userId };
  }
  else if (req.currentUser) {
    filter.where["userId"] = { like: CurrentUser };
  }

  videos = await Video.find(filter);
  return videos;
}

async function GetVotedVideos(videos, filter, CurrentUser) {
  var videoIds = videos.map((item) => {
    return item.id;
  });
  // 
  var filter = {
    and: [{
      videoId: {
        inq: videoIds
      }
    }, {
      userId: {
        like: CurrentUser
      }
    }, {
      type: 'vote'
    }]
  };
  var votedVideos = [];
  try {
    var votes = await Indicators.find({ where: filter });
    votedVideos = votes.map((item) => {
      return item.videoId;
    });
  }
  catch (error) {
  }

  return votedVideos;
}

function MapResult(videos, users, votedVideos) {
  return videos.map(element => {
    // 
    var obj = {
      video: element,
      user: users.find((param) => {
        //
        return param.id == element.userId;
      }), isVoted: votedVideos.includes(element.id)
    };
    //
    return obj;
  });
}

